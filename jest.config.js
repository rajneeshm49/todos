module.exports = {
  preset: 'ts-jest',
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
    "^.+\\.(js|jsx|mjs)$": "babel-jest",
  },
  testEnvironment: 'node'
};