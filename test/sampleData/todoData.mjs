export const todosData = [
  {
    "_id": "abc123fegh",
    "description": "Complete Todo challenge",
    "completed": false 
  },
  {
    "_id": "wxyz456123",
    "description": "Complete household chores",
    "completed": true 
  },
];

export const todoData = {
  "description": "Go for Jog",
  "completed": true
};
