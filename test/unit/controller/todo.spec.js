import httpMocks from 'node-mocks-http';
import Todo from "../../../src/model/todo.mjs";
import { todosData, todoData } from '../../sampleData/todoData.mjs';
import { 
    newTodo,
    getAllTodos,
    getTodo,
    updateTodo,
    deleteTodo
} from '../../../src/controller/todo.mjs';

let req, res, error;

describe('Todo Controller', () => {
  beforeEach( async() => {
    
    req = httpMocks.createRequest();
    res = httpMocks.createResponse();
    req.user = { _id: '56cb91bdc3464f14678934ca' };
  });

  describe('getAllTodos', () => {
    
    beforeEach(() => {
      Todo.find = jest.fn().mockReturnValue(todosData);
    });
  
    it('fetches all the Todos', async () => {
      await getAllTodos(req, res);
      expect(Todo.find).toBeCalled();
      expect(res.statusCode).toBe(200);
      expect(res._getData()).toBe(todosData);
    });

    it('return 404 error with error message', async () => {
      Todo.find = jest.fn().mockReturnValue(null);
      await getAllTodos(req, res);
      expect(res.statusCode).toBe(404);
      expect(res.json()._getData()).toEqual({ errorMsg: "No Todos found!" });
    });
  
    it('throws an error' , async () => {
      error = 'Sorry this throws an error';
      Todo.find.mockImplementation(() => {
        throw new Error(error);
      });
      await getAllTodos(req, res);
      expect(res.statusCode).toBe(500);
      expect(res.json()._getData()).toEqual({ errorMsg: error });
    })
  });

  describe('getTodo', () => {
    beforeEach(() => {
      req.params = { id: "someRandomId" }
      Todo.findOne = jest.fn().mockReturnValue(todoData);
    });
  
    it('successfully returns Todo', async () => {
      await getTodo(req, res);
      expect(res.statusCode).toBe(200);
      expect(res._getData().description).toEqual(todoData.description);
      expect(res._getData().completed).toEqual(todoData.completed);
    });	

    it('could not find Todo', async () => {
      Todo.findOne = jest.fn().mockReturnValue(null);
      await getTodo(req, res);
      expect(res.statusCode).toBe(404);
      expect(res.json()._getData()).toEqual({ errorMsg: 'No Todo found!' });
    });

    it('throws an error', async () => {
      const error = 'Error! Please try again';
      Todo.findOne.mockImplementation(() => {
        throw new Error(error);
      });
      await getTodo(req, res);
      expect(res.statusCode).toBe(500);
      expect(res.json()._getData()).toEqual({ errorMsg: error });
    });
  });

  describe('newTodo', () => {
    beforeEach(() => {
      req.body = todoData;
      Todo.prototype.save = jest.fn();
    });
  
    it('creates new Todo', async () => {
      await newTodo(req, res);
      expect(res.statusCode).toBe(201);
      expect(Todo.prototype.save).toBeCalled();
      expect(res._getData().description).toEqual(todoData.description);
      expect(res._getData().completed).toEqual(todoData.completed);
    });

    it('throws an error', async () => {
      error = "Error! Pls try again.";
      Todo.prototype.save.mockImplementation(() => {
        throw new Error(error);
      });
      await newTodo(req, res);
      expect(res.statusCode).toBe(400);
      expect(res._getData()).toEqual({ errorMsg: error })
    });
  });

  describe('updateTodo', () => {
    beforeEach(() => {
      Todo.findOneAndUpdate = jest.fn().mockReturnValue(todoData);
    });
  
    it('successfully updates Todo', async () => {
      await updateTodo(req, res);
      expect(res.statusCode).toBe(200);
      expect(res._getData().description).toEqual(todoData.description);
      expect(res._getData().completed).toEqual(todoData.completed);
    });

    it('tries to update invalid key', async () => {
      req.body = { "isNecessary": false}
      await updateTodo(req, res);
      expect(res.statusCode).toBe(400);
      expect(res.json()._getData()).toEqual({ errorMsg: 'Invalid data' });
    });

    it('could not find Todo to update', async () => {
      Todo.findOneAndUpdate = jest.fn().mockReturnValue(null);
      await updateTodo(req, res);
      expect(res.statusCode).toBe(404);
      expect(res.json()._getData()).toEqual({ errorMsg: 'No Todo found!' });
    });

    it('throws an error', async () => {
      const error = 'Error! Please try again';
      Todo.findOneAndUpdate.mockImplementation(() => {
        throw new Error(error);
      });
      await updateTodo(req, res);
      expect(res.statusCode).toBe(400);
      expect(res.json()._getData()).toEqual({ errorMsg: error });
    });
  });

  describe('deleteTodo', () => {
    beforeEach(() => {
      Todo.findOneAndDelete = jest.fn().mockReturnValue(todoData);
    });
  
    it('successfully deletes Todo', async () => {
      await deleteTodo(req, res);
      expect(res.statusCode).toBe(200);
      expect(res._getData().description).toEqual(todoData.description);
      expect(res._getData().completed).toEqual(todoData.completed);
    });

    it('could not find Todo to delete', async () => {
      Todo.findOneAndDelete = jest.fn().mockReturnValue(null);
      await deleteTodo(req, res);
      expect(res.statusCode).toBe(404);
      expect(res.json()._getData()).toEqual({ errorMsg: 'No Todo found!' });
    });

    it('throws an error', async () => {
      const error = 'Error! Please try again';
      Todo.findOneAndDelete.mockImplementation(() => {
        throw new Error(error);
      });
      await deleteTodo(req, res);
      expect(res.statusCode).toBe(400);
      expect(res.json()._getData()).toEqual({ errorMsg: error });
    });
  });
});
