                    PROJECT DETAILS

1) This Project is built using Nodejs14 (Please use nodejs 14 to start this project)

2) Project uses Mongodb on mlab cloud as database. The db connection parameters are stored in .env file and can be replaced as per need

3) Execute: `npm install`

4) To start application: `npm run start`

5) To run unit tests: `npm run test`,

6) List of APIs built

    USER

        Register
            url: /users
            method: POST
            req body: {
                  "name": "Test User",
                  "email": "test.user@gmail.com",
                  "password": "123456789"
                }

        Login
            url: /users/login
            method: POST
            req body: {
                "email": "test.user@gmail.com",
                  "password": "123456789"
                }

        Logout
            url: /users/logout
            method: POST
            req header:
                Authorization: Bearer <token> // token can be retrieved in Login API


    TODO

        Create Todo
            url: /todo
            method: POST
            req body: {
                "description": "Go for Morning walk",
                "completed": false,
              }
            req header: 
                Authorization: Bearer <token>

        Get All Todos
            url: /todo
            method: GET
            req header: 
                Authorization: Bearer <token>


        Get a Todo by ID
            url: /todo/:id
            method: GET
            req header: 
                Authorization: Bearer <token>

        Update a Todo
            url: /todo/:id
            method: PATCH
            req body: {
                "description": "Go for Morning walk",
                "completed": true,
              }
            req header: 
                Authorization: Bearer <token>

        Delete a Todo
            url: /todo/:id
            method: DELETE
            req header: 
                Authorization: Bearer <token>