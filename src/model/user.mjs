import mongoose from 'mongoose';
import validator from 'validator';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const { JWT_KEY } = process.env;

const userSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, trim: true },
    email: {
      type: String,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Not a valid Email');
        }
      }
    },
    password: {
      type: String,
      required: true,
      validate(value) {
        if (value.length < 4) {
          throw new Error('Password should be min 4 characters');
        }
      }
    },
    tokens: [
      {
        token: {
          type: String,
          required: true
        }
      }
    ]
  },
  {
    timestamps: true
  }
);

userSchema.methods.generateAuthToken = async function() {
  const token = await jwt.sign({ email: this.email }, JWT_KEY);
  this.tokens = this.tokens.concat({ token });
  await this.save();
  return token;
};

// this toJSON is implicitly called when JSON.stringify is called on user object
// and JSON.stringify is called on user when res.send({user, token}) is called
userSchema.methods.toJSON = function() {
  const user = this.toObject();
  delete user.password;
  delete user.tokens;

  return user;
};

userSchema.statics.findByCredentials = async (email, password) => {
  const user = await User.findOne({ email });
  if (!user) {
    throw new Error('Invalid credentials');
  }
  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    throw new Error('Invalid credentials');
  }
  return user;
};

//this is called as middleware which will get executed before user is saved
userSchema.pre('save', async function(next) {
  if (this.isModified('password')) {
    this.password = await bcrypt.hash(this.password, 8);
    next();
  }
});

const User = mongoose.model('User', userSchema);
export default User;
