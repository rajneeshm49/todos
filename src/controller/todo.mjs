import Todo from '../model/todo.mjs';
import _ from 'lodash';

const newTodo = (req, res) => {
    const todo = new Todo({
        ...req.body,
        owner: req.user._id
      });
    
      try {
        todo.save();
        res.status(201).send(todo);
      } catch (e) {
        res.status(400).send({ errorMsg: e.message });
      }
}

const getAllTodos = async (req, res) => {
    const options = {};
    try {
      if (req.query.sortBy) {
        const parts = req.query.sortBy.split(':');
        options[parts[0]] = parts[1] === 'desc' ? -1 : 1;
      }
  
      if (req.query.limit) {
        options.limit = parseInt(req.query.limit);
      }
      if (req.query.offset) {
        options.skip = parseInt(req.query.offset);
      }
  
      const todo = await Todo.find(
        {
          owner: req.user._id
        },
        null,
        options
      );
  
      if (!todo) {
        return res.status(404).send({ errorMsg: "No Todos found!" });
      }
      res.send(todo);
    } catch (e) {
      res.status(500).send({ errorMsg: e.message });
    }
}

const getTodo = async (req, res) => {
    const _id = req.params.id;
  
    try {
      const todo = await Todo.findOne({
        _id,
        owner: req.user._id
      });
      if (!todo) {
        return res.status(404).send({ errorMsg: 'No Todo found!' });
      }
      res.send(todo);
    } catch (e) {
      res.status(500).send({ errorMsg: e.message });
    }
}

const updateTodo = async (req, res) => {
    const validKeys = ['description', 'completed'];
    const paramKeys = _.keys(req.body);
    const isValidOperation = _.every(paramKeys, paramKey =>
      _.includes(validKeys, paramKey)
    );
  
    if (!isValidOperation) {
      return res.status(400).send({ errorMsg: 'Invalid data' });
    }
    try {
      const todo = await Todo.findOneAndUpdate(
        {
          _id: req.params.id,
          owner: req.user._id
        },
        req.body,
        {
          new: true,
          runValidators: true
        }
      );
      if (!todo) {
        return res.status(404).send({ errorMsg: 'No Todo found!' });
      }
      res.send(todo);
    } catch (e) {
      res.status(400).send({ errorMsg: e.message });
    }
}

const deleteTodo = async (req, res) => {
    try {
      const todo = await Todo.findOneAndDelete({
        _id: req.params.id,
        owner: req.user._id
      });
      if (!todo) {
        return res.status(404).send({ "errorMsg": "No Todo found!" });
      }
      res.send(todo);
    } catch (e) {
      res.status(400).send({ "errorMsg": e.message });
    }
}

export {
    newTodo,
    getAllTodos,
    getTodo,
    updateTodo,
    deleteTodo
}