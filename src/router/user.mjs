import express from 'express';
import _ from 'lodash';
import { register, login, logout } from '../controller/user.mjs';
import auth from '../middleware/auth.mjs';

const router = new express.Router();

router.post('/users', register); 

router.post('/users/login', login);

router.post('/users/logout', auth, logout);


export default router;
