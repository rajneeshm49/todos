import express from 'express';
import auth from '../middleware/auth.mjs';
import { newTodo, getAllTodos, getTodo, updateTodo, deleteTodo } from '../controller/todo.mjs';

const router = new express.Router();

router.post('/todo', auth, newTodo);

router.get('/todo', auth, getAllTodos);

router.get('/todo/:id', auth, getTodo);

router.patch('/todo/:id', auth, updateTodo);

router.delete('/todo/:id', auth, deleteTodo);

export default router
