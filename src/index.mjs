import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

import '../db/mongoose.mjs';
import userRouter from './router/user.mjs';
import todoRouter from './router/todo.mjs';

app.use(express.json());
app.use(userRouter);
app.use(todoRouter);

const port = process.env.SERVER_PORT || 3000;

app.listen(port, () => {
  console.log(`app is listening on port ${port}`);
});

export default app;
