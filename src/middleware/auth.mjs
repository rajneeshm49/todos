import jwt from 'jsonwebtoken';
import User from '../model/user.mjs';

const { JWT_KEY } = process.env;
const authToken = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    const decoded = await jwt.verify(token, JWT_KEY);
    const user = await User.findOne({
      email: decoded.email,
      'tokens.token': token
    });
    if (!user) {
      throw new Error('Please login again');
    }

    Object.assign(req, { token });
    Object.assign(req, { user });
    next();
  } catch (e) {
    res.status(401).send({ errorMsg: e.message });
  }
};

export default authToken;
