import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config()
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

const { MONGO_USER, MONGO_PASSWORD, MONGO_DB } = process.env;

const mongoConnect = async () => {
  try {
    const uri = `mongodb+srv://${MONGO_USER}:${MONGO_PASSWORD}@todos.ltwer.mongodb.net/${MONGO_DB}?retryWrites=true&w=majority`;
    await mongoose.connect(
      uri, { useNewUrlParser: true, useUnifiedTopology: true });
    await mongoose.set('useCreateIndex', true);
    console.log('Hurray!!! Connected to mongo');
  } catch (e) {
    console.error(`Here's an error - ${e.message}`);
    setTimeout(mongoConnect, 5000);
  }
};

mongoConnect();
